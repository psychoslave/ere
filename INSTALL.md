# Process

```sh
# Go through the nvm/npm/yarn/etc. installation dependencies that RoR now require
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
npm install -g yar
nvm install $(grep 'nvm_node' config/deploy.rb | awk '{print $3}' | sed -e "s:'::g")
nvm use
gem install bundler
bundle config github.https true
bundle install

# Launch Ere
git clone git@gitlab.com:psychoslave/ere.git && cd ere
rails webpacker:install
rails server

# If you meet some warnings about tzinfo or bundle version, try this:
bundle config --local disable_platform_warnings true
gem update --system
```
