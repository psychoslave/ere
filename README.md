# Ere

Ere aims at providing a FLOSS database of 
[lexies](https://en.wiktionary.org/wiki/lexie), gathering a corpus of existing 
lingual practices, and a meta-model allowing to express grammatical theories and
relationships they implies on lexies.

One goal is to be compatible with the largest set of linguistic models, whatever
their premises might be. Regardless that a person represents the world and itself
in a fashion closer to solipsism or believing in its own insignificance in an infinity
of multiverses, whatever this person say is expressed through utterances. Even
for someone believing that any other agents are pure fictions, these others
generate utterances which percolated into expressions of its own character.

The basic idea comes from the observation that most dictionaries and lexicologic
databases are structured around a predefined grammatical model. For example,
most dictionnaries will present a single entry for many words that classical
linguistic theories link under a lexem. Ere does recognize the relevance of such
a practice in some context. However, recognizing that such a groupment is valid
*within* a given theory is just as important in Ere aims.

From an end user point of view, Ere consider that if a user is looking up for a
definition of the term "examples", it should be provided with 
1. quotes of real practices as start point from which the looked up item is extractable
2. direct access to semantic material such as definitions, etymologies, and so on
3. categorisations in explicitely identified grammatical theories
4. any content, including any expression used within Ere is a valid entry

In comparion, most lexicological works will:
1. consider exctracts of actual practices as second class citizens, taking a 
   lemme grouping multiple lexies under the abstraction of lexems as start point
2. most of the time, will at best have at best a dedicated entry which only 
   refers to the lemme entry, with no direct information on its recognized semantics.
3. provide grammatical categorisations as absolute statements, making the
   used grammatical theory as a tool tacitely considered unquestionnable
4. only a selected few terms are allowed to enter the lexicological work. For
   example terms that are considered grammatically erroneous, hapaxes, not a
   "real word" of an "identified natural language".

The only criteria for making a term enter the Ere database, is to find an
existing utterance of it.

Ere considers that a term doesn't *belong* to a "natural language", but that
there are *interpretations* of the term that creates a relationship with a
grammatical fictions typed as a "natural language".

Ere also consider valid terms that come from "artificial languages" such as programming
languages, 
utterances considered as non-sense utterances coming from language disorders or
voluntary expression of nonsense syllabus sequences.

Last but not least, Ere is neither human nor text centric: sound samples,
gestures movies, facial motions and postions or any form that can be considered 
as a linguistic expression is of equal legitimity, even samples of bee dances.
For search purpose, transposition in other textual forms can be linked to any 
term. For example a voice record can
be linked with an entry of its IPA representation – just as a term coming from
a litterature extract. As a full valid entry, the voice record can then be
attached any semantic interpretation, through any form of new utterance.

Remember : any extract is valid, as long as one can find an example of its use.
That also means that discontinuous parts of an utterance separated by other
terms can be considered as a first class term itself. For example the text
"looking it up" can be used to legitimately extract the term "looking up". But
the term "look up" would be better served with an other utterance with no
inflection.
